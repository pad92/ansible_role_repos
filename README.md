# cts_role_repos

Set official and custom repository

## Requirements

none

## Role Variables

```
cts_role_repos:
  default: false
  mirror: repo.exemple.local
  additional:
  - id: elastic-5.x
    name: Elastic repository for 5.x packages
    url: http://repo.exemple.local/elastic-5.x
    gpgcheck: 0
    enabled: 0
    autorefresh: 1
  - id: HDP-2.5.3.0
    name: HDP Version - HDP-2.5.3.0
    url: http://repo.exemple.local/hdp/HDP-2.5.3.0
    gpgcheck: 0
    enabled: 0
    autorefresh: 1
```

- default: for use defaults mirror or not (CentOS only)
- mirror: set mirror url for OS repository (CentOS only)
- additional: add repository (CentOS only)

For apt like repository, use /etc/apt/sources.list.d/ directory for additional repository

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - repos
```

